#ifndef department_h
#define department_h

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>

    typedef struct t_department {
        int id;
        char * label;
        struct t_department * next;
    } t_department;

    t_department * department_form(t_department * departments_root);
    int add_department(t_department ** departments_root, t_department * new_department);
    int modify_department(t_department ** departments_root, int id);
    int delete_department(t_department ** departments_root, int id);
    void show_department(t_department * departments_root, int id);
    void list_departments(t_department * departments_root);

#endif // !department_h
