#ifndef role_h
#define role_h

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>

    typedef struct t_role {
        int id;
        char * label;
        struct t_role * next;
    } t_role;

    t_role * role_form(t_role * roles_root);
    int add_role(t_role ** roles_root, t_role * new_role);
    int modify_role(t_role ** roles_root, int id);
    int delete_role(t_role ** roles_root, int id);
    void show_role(t_role * roles_root, int id);
    void list_roles(t_role * roles_root);

#endif // !role_h