#include "readfile.h"

/**
 * @def readfile
 * read a file and and 
 * create an object of its content
 * @param FILE * file
 * @return FileContent * 
*/
FileContent * readfile(FILE * file) {
    char single_line [1000];
    FileContent * file_content = NULL;

    int line_count = 0;
    while( fgets(single_line, 255, file) ) {
        if(strlen(single_line) != 0) {
            add_file_content(&file_content, single_line);
            line_count++;
        }
    }
    if(line_count)
        return file_content;
    return NULL;
}

/**
 * add a line data file content object
 * @def add_file_content
 * @param FileContent * listHead
 * @param char * data
 * @return void 
*/
void add_file_content(FileContent ** file_content, char * data) {

    if(*file_content == NULL) {
        *file_content = (FileContent*) malloc(sizeof(FileContent));
        (*file_content)->data = (char *) malloc(strlen(data) * sizeof(char));
        strcpy((*file_content)->data, data);
        (*file_content)->next = NULL;
    } else {
        FileContent * temp = *file_content;
        while(temp->next != NULL) {
            temp = temp->next;
        }

        FileContent * next = (FileContent*) malloc(sizeof(FileContent));
        next->data = (char *) malloc(strlen(data) * sizeof(char));
        strcpy(next->data, data);
        next->next = NULL;

        temp->next = next;
    }

}