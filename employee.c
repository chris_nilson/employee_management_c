#include <stdio.h>
#include "employee.h"
#include "init.h"
#define MAX_NAME_SIZE 100

t_employee * employee_form(t_employee * employees_root) {

    int id = 1;
    if(employees_root != NULL) {
        t_employee * current = employees_root;
        while (current->next != NULL)
        {
            id++;
            current = current->next;
        }
        id += 1;
    }


    t_employee * new_employee = (t_employee*) malloc(sizeof(t_employee));
    new_employee->id = id;
    new_employee->next = NULL;

    new_employee->firstname = (char*) malloc(sizeof(char));
    new_employee->lastname = (char*) malloc(sizeof(char));
    new_employee->address = (char*) malloc(sizeof(char));
    new_employee->phone = (char*) malloc(sizeof(char));
    new_employee->email = (char*) malloc(sizeof(char));
    new_employee->role_id = 1;
    new_employee->department_id = 1;

    printf("Firstname: ");
    fgets(new_employee->firstname, MAX_NAME_SIZE, stdin);
    new_employee->firstname = strremoveone('\n', new_employee->firstname);

    printf("Lastname: ");
    fgets(new_employee->lastname, MAX_NAME_SIZE, stdin);
    new_employee->lastname = strremoveone('\n', new_employee->lastname);

    printf("Address: ");
    fgets(new_employee->address, MAX_NAME_SIZE, stdin);
    new_employee->address = strremoveone('\n', new_employee->address);

    printf("Phone: ");
    fgets(new_employee->phone, MAX_NAME_SIZE, stdin);
    new_employee->phone = strremoveone('\n', new_employee->phone);

    printf("Email: ");
    fgets(new_employee->email, MAX_NAME_SIZE, stdin);
    new_employee->email = strremoveone('\n', new_employee->email);

    printf("Department (id only): ");
    scanf("%d", &new_employee->department_id);
    getchar();

    printf("Role (id only): ");
    scanf("%d", &new_employee->role_id);
    getchar();
    
    return new_employee;

}

int add_employee(t_employee ** employees_root, t_employee * new_employee) {

    if(*employees_root == NULL) {
        *employees_root = (t_employee*) malloc(sizeof(t_employee));
        *employees_root = new_employee;
        (*employees_root)->next = NULL;
        return new_employee->id;
    }

    t_employee * current = *employees_root;
    t_employee * current_prev = *employees_root;

    // put at the bigining
    if(strcmp(new_employee->lastname, (*employees_root)->lastname) <= 0 ) {
        new_employee->next = *employees_root;
        *employees_root = new_employee;
        return new_employee->id;
    }

    while (current != NULL && strcmp(current->lastname, new_employee->lastname) < 0)
    {
        current_prev = current;
        current = current->next;
    }

    // put in the middle
    if(current != NULL) {
        new_employee->next = current;
        current_prev->next = new_employee;
    
        return new_employee->id;
    }

    // push at the end of the list
    if(current == NULL) {
        current_prev->next = new_employee;

        return new_employee->id;
    }
    return new_employee->id;

}

int modify_employee(t_employee ** employees_root, int id) {
    t_employee * new_employee = employee_form(*employees_root);

    t_employee * current = *employees_root;
    while (current != NULL && current->id != id)
    {
        current = current;
        current = current->next;
    }

    if(current == NULL) {
        printf("\033[1;31m[not found] row with id: #%d not found\033[m\n", id);
        return -1; // employee not found
    }

    new_employee->id = current->id;
    current->firstname = new_employee->firstname;
    current->lastname = new_employee->lastname;
    current->address = new_employee->address;
    current->phone = new_employee->phone;
    current->email = new_employee->email;

    printf("\033[1;32m Employee updated successfully\033[m\n");

    return new_employee->id;
}

int delete_employee(t_employee ** employees_root, int id) {
    t_employee * current = *employees_root;
    t_employee * current_next = *employees_root;

    while (current_next != NULL && current_next->id != id)
    {
        current = current_next;
        current_next = current_next->next;
    }

    if(current_next == NULL) {
        printf("\033[1;31m[not found] row with id: #%d not found\033[m\n", id);
        return -1; // employee not found
    }

    t_employee * to_delete = current_next;
    current->next = current_next->next;
    free(to_delete);

    printf("\033[1;32m Employee deleted successfully\033[m\n");
    return id;
}

void show_employee(t_employee * employees_root, int id) {

    printf("\033[1;4;43m-- Employee #%d --\033[m\n", id);
    printf("------------------------------------------------------------------------------------------\n");
    printf("  \033[1;31m#ID\t| LASTNAME\tFIRSTNAME\tADDRESS\tPHONE\tEMAIL\tDEPARTMENT\tROLE\033[m\n");
    printf("------------------------------------------------------------------------------------------\n");

    t_employee * current = employees_root;
    while (current != NULL && current->id != id)
    {
        current = current->next;
    }

    if(current == NULL) {
        printf("\033[1;31m[not found] row with id: #%d not found\033[m\n", id);
    } else {
        printf("  \033[1;31m#%d\033[m\t| %s\t%s\t%s\t%s\t%s\t%d\t%d\n", 
            current->id, 
            current->lastname,
            current->firstname,
            current->address,
            current->phone,
            current->email,
            current->department_id,
            current->role_id
        );
        printf("------------------------------------------------------------------------------------------\n");
 
    }


}

void list_employees(t_employee * employees_root) {

    printf("\033[1;4;43m-- Employees --\033[m\n");
    printf("------------------------------------------------------------------------------------------\n");
    printf("  \033[1;31m#ID\t| LASTNAME\tFIRSTNAME\tADDRESS\tPHONE\tEMAIL\tDEPARTMENT\tROLE\033[m\n");
    printf("------------------------------------------------------------------------------------------\n");

    t_employee * current = employees_root;
    while (current != NULL)
    {
        printf("  \033[1;31m#%d\033[m\t| %s\t%s\t%s\t%s\t%s\t%d\t%d\n", 
            current->id, 
            current->lastname,
            current->firstname,
            current->address,
            current->phone,
            current->email,
            current->department_id,
            current->role_id
        );
        printf("------------------------------------------------------------------------------------------\n");
        current = current->next;
    }
}

void listing_per_role(t_employee * employees_root, t_role * role_root) {

    t_role * current_role = role_root;
    while (current_role != NULL)
    {
        printf("\n\033[1;4;43m-- Employees with role: %s --\033[m\n", current_role->label);
        printf("------------------------------------------------------------------------------------------\n");
        printf("  \033[1;31m#ID\t| LASTNAME\tFIRSTNAME\tADDRESS\tPHONE\tEMAIL\tDEPARTMENT\tROLE\033[m\n");
        printf("------------------------------------------------------------------------------------------\n");
    
        t_employee * current = employees_root;
        while (current != NULL)
        {
            if(current->role_id == current_role->id) {
                printf("  \033[1;31m#%d\033[m\t| %s\t%s\t%s\t%s\t%s\t%d\t%d\n", 
                    current->id, 
                    current->lastname,
                    current->firstname,
                    current->address,
                    current->phone,
                    current->email,
                    current->department_id,
                    current->role_id
                );
                printf("------------------------------------------------------------------------------------------\n");
            }
            current = current->next;
        }
        current_role = current_role->next;
    }
}

void listing_per_department(t_employee * employees_root, t_department * department_root) {

    t_department * current_departent = department_root;
    while (current_departent != NULL)
    {
        printf("\n\033[1;4;43m-- Employees from department: %s --\033[m\n", current_departent->label);
        printf("------------------------------------------------------------------------------------------\n");
        printf("  \033[1;31m#ID\t| LASTNAME\tFIRSTNAME\tADDRESS\tPHONE\tEMAIL\tDEPARTMENT\tROLE\033[m\n");
        printf("------------------------------------------------------------------------------------------\n");
    
        t_employee * current = employees_root;
        while (current != NULL)
        {
            if(current->department_id == current_departent->id) {
                printf("  \033[1;31m#%d\033[m\t| %s\t%s\t%s\t%s\t%s\t%d\t%d\n", 
                    current->id, 
                    current->lastname,
                    current->firstname,
                    current->address,
                    current->phone,
                    current->email,
                    current->department_id,
                    current->role_id
                );
                printf("------------------------------------------------------------------------------------------\n");
            }
            current = current->next;
        }
        current_departent = current_departent->next;
    }
}
