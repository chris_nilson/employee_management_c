#ifndef employee_h
#define employee_h

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include "role.h"
    #include "department.h"

    typedef struct t_employee {
        int id;
        char * firstname;
        char * lastname;
        char * address;
        char * phone;
        char * email;
        int department_id;
        int role_id;
        struct t_employee * next;
    } t_employee;

    t_employee * employee_form(t_employee * employees_root);
    int add_employee(t_employee ** employees_root, t_employee * new_employee);
    int modify_employee(t_employee ** employees_root, int id);
    int delete_employee(t_employee ** employees_root, int id);
    void show_employee(t_employee * employees_root, int id);
    void list_employees(t_employee * employees_root);
    void listing_per_role(t_employee * employee_root, t_role * role_root);
    void listing_per_department(t_employee * employee_root, t_department * department_root);

#endif // !employee_h
