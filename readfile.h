#ifndef READFILE_H
#define READFILE_H

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>

    /**
     * define a line of a file and target the next line
     * @struct FileContent
     * @property char * data
     * @property FileContent * next
    */
    typedef struct FileContent {
        char * data; // one line of data read from a file 
        struct FileContent * next; // target to the next line
    } FileContent;

    /**
     * @def : prototypes
    */
    FileContent * readfile(FILE *);
    void add_file_content(FileContent **, char *);

#endif // !READFILE_H