#include "role.h"
#include "init.h"
#define MAX_NAME_SIZE 100


t_role * role_form(t_role * roles_root) {

    int id = 1;
    if(roles_root != NULL) {
        t_role * current = roles_root;
        while (current->next != NULL)
        {
            id++;
            current = current->next;
        }
        id += 1;
    }

    t_role * new_role = (t_role*) malloc(sizeof(t_role));
    new_role->id = id;
    new_role->next = NULL;

    new_role->label = (char*) malloc(sizeof(char));

    printf("Role label: ");
    fgets(new_role->label, MAX_NAME_SIZE, stdin);
    new_role->label = strremoveone('\n', new_role->label);
    
    return new_role;

}

int add_role(t_role ** roles_root, t_role * new_role) {

    if(*roles_root == NULL) {
        *roles_root = (t_role*) malloc(sizeof(t_role));
        *roles_root = new_role;
        return new_role->id;
    }

    t_role * current = *roles_root;
    while (current->next != NULL)
    {
        current = current->next;
    }

    current->next = (t_role*) malloc(sizeof(t_role));
    current->next = new_role;
    return new_role->id;

}

int modify_role(t_role ** roles_root, int id) {
    t_role * new_role = role_form(*roles_root);

    t_role * current = *roles_root;
    while (current != NULL && current->id != id)
    {
        current = current;
        current = current->next;
    }

    if(current == NULL) {
        printf("\033[1;31m[not found] row with id: #%d not found\033[m\n", id);
        return -1; // role not found
    }

    new_role->id = current->id;
    current->label = new_role->label;

    printf("\033[1;32m Role updated successfully\033[m\n");
    return new_role->id;
}

int delete_role(t_role ** roles_root, int id) {
    t_role * current = *roles_root;
    t_role * current_next = *roles_root;

    while (current_next != NULL && current_next->id != id)
    {
        current = current_next;
        current_next = current_next->next;
    }

    if(current_next == NULL) {
        printf("\033[1;31m[not found] row with id: #%d not found\033[m\n", id);
        return -1; // role not found
    }

    t_role * to_delete = current_next;
    current->next = current_next->next;
    free(to_delete);
    
    printf("\033[1;32m Role deleted successfully\033[m\n");
    return id;
}

void show_role(t_role * roles_root, int id) {

    printf("\033[1;4;43m-- Role #%d --\033[m\n", id);
    printf("-----------------------------------\n");
    printf("  \033[1;31m#ID\t|\tLABEL\033[m\n");
    printf("-----------------------------------\n");

    t_role * current = roles_root;
    while (current != NULL && current->id != id)
    {
        current = current->next;
    }

    if(current == NULL) {
        printf("\033[1;31m[not found] row with id: #%d not found\033[m\n", id);
    } else {
        printf("  \033[1;31m#%d\033[m\t| %s\n", current->id, current->label);
        printf("-----------------------------------\n");
    }


}

void list_roles(t_role * roles_root) {

    printf("\033[1;4;43m-- Roles --\033[m\n");
    printf("-----------------------------------\n");
    printf("  \033[1;31m#ID\t|\tLABEL\033[m\n");
    printf("-----------------------------------\n");
    t_role * current = roles_root;
    while (current != NULL)
    {
        printf("  \033[1;31m#%d\033[m\t| %s\n", current->id, current->label);
        printf("-----------------------------------\n");
        current = current->next;
    }
}