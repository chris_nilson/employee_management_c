#include "department.h"
#include "init.h"
#define MAX_NAME_SIZE 100

t_department * department_form(t_department * departments_root) {

    int id = 1;
    if(departments_root != NULL ) {
        t_department * current = departments_root;
        while (current->next != NULL)
        {
            id++;
            current = current->next;
        }
        id += 1;
    }

    t_department * new_department = (t_department*) malloc(sizeof(t_department));
    new_department->id = id;
    new_department->next = NULL;

    new_department->label = (char*) malloc(sizeof(char));

    printf("Department Label: ");
    fgets(new_department->label, MAX_NAME_SIZE, stdin);
    new_department->label = strremoveone('\n', new_department->label);
    
    return new_department;

}

int add_department(t_department ** departments_root, t_department * new_department) {

    if(*departments_root == NULL) {
        *departments_root = (t_department*) malloc(sizeof(t_department));
        *departments_root = new_department;
        return new_department->id;
    }

    t_department * current = *departments_root;
    while (current->next != NULL)
    {
        current = current->next;
    }

    current->next = (t_department*) malloc(sizeof(t_department));
    current->next = new_department;

    return new_department->id;

}

int modify_department(t_department ** departments_root, int id) {
    t_department * new_department = department_form(*departments_root);

    t_department * current = *departments_root;
    while (current != NULL && current->id != id)
    {
        current = current;
        current = current->next;
    }

    if(current == NULL) {
        printf("\033[1;31m[not found] row with id: #%d not found\033[m\n", id);
        return -1; // department not found
    }

    new_department->id = current->id;
    current->label = new_department->label;

    printf("\033[1;32m Department updated successfully\033[m\n");
    return new_department->id;
}

int delete_department(t_department ** departments_root, int id) {
    t_department * current = *departments_root;
    t_department * current_next = *departments_root;

    while (current_next != NULL && current_next->id != id)
    {
        current = current_next;
        current_next = current_next->next;
    }

    if(current_next == NULL) {
        printf("\033[1;31m[not found] row with id: #%d not found\033[m\n", id);
        return -1; // department not found
    }

    t_department * to_delete = current_next;
    current->next = current_next->next;
    free(to_delete);
    
    printf("\033[1;32m Department deleted successfully\033[m\n");
    return id;
}

void show_department(t_department * departments_root, int id) {
    
    printf("\033[1;4;43m-- Department #%d --\033[m\n", id);
    printf("-----------------------------------\n");
    printf("  \033[1;31m#ID\t|\tLABEL\033[m\n");
    printf("-----------------------------------\n");

    t_department * current = departments_root;
    while (current != NULL && current->id != id)
    {
        current = current->next;
    }

    if(current == NULL) {
        printf("\033[1;31m[not found] row with id: #%d not found\033[m\n", id);
    } else {
        printf("  \033[1;31m#%d\033[m\t| %s\n", current->id, current->label);
        printf("-----------------------------------\n");
    }


}

void list_departments(t_department * departments_root) {

    printf("\033[1;4;43m-- Departments --\033[m\n");
    printf("-----------------------------------\n");
    printf("  \033[1;31m#ID\t|\tLABEL\033[m\n");
    printf("-----------------------------------\n");

    t_department * current = departments_root;
    while (current != NULL)
    {
        printf("  \033[1;31m#%d\033[m\t| %s\n", current->id, current->label);
        printf("-----------------------------------\n");
        current = current->next;
    }
}