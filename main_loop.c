#include "init.h"
#include "role.h"
#include "employee.h"
#include "department.h"

void main_loop() {
    
    int choice = -1;
    t_department *department_head = NULL;
    t_employee *employee_head = NULL;
    t_role *role_head = NULL;
    init(&employee_head, &department_head, &role_head);


    do {

        switch (choice)
        {

        case -1: {
            printf("\n\033[1;3;32m----------- Options ------------\n");
            printf(" 10 - Departments Submenu \n 20 - Roles Submenu \n 30 - Employees Submenu \n 40 - Advanced Listing Submenu \n 00 - Quit\n");
            printf("--------------------------------\033[m\n\n");
            break;
        }

            // submenu
        case 10: {
            printf("\033[1;4;43m-- Départments Submenu -- \033[m\n");
            printf("---------------------------------------------------------\n");
            printf(" 11- add \t\t 12- show \t 13- list\n");
            printf(" 14- update \t\t 15- delete\n");
            printf("---------------------------------------------------------\n");
            break;
        }
        case 20: {
            printf("\033[1;4;43m-- Roles Submenu -- \033[m\n");
            printf("---------------------------------------------------------\n");
            printf(" 21- add \t\t 22- show \t 23- list\n");
            printf(" 24- update \t\t 25- delete\n");
            printf("---------------------------------------------------------\n");
            break;
        }
        case 30: {
            printf("\033[1;4;34;43m-- Employees Submenu -- \033[m\n");
            printf("---------------------------------------------------------\n");
            printf(" 31- add \t\t 32- show \t 33- list\n");
            printf(" 34- update \t\t 35- delete\n");
            printf("---------------------------------------------------------\n");
            break;
        }

        case 40: {
            printf("\033[1;4;43m-- Advanced Listing Submenu -- \033[m\n");
            printf("---------------------------------------------------------\n");
            printf(" 41- list by role \t\t 42- list by department\n");
            printf("---------------------------------------------------------\n");
            break;
        }
        
            // add
        case 11 : {
            t_department * new_department = department_form(department_head);
            add_department(&department_head, new_department);
            printf("\033[1;32m Department added successfully\033[m\n");
            break;
        }
        case 21: {
            t_role * new_role = role_form(role_head);
            add_role(&role_head, new_role);
            printf("\033[1;32m Role addes successfully\033[m\n");
            break;
        }
        case 31: {
            t_employee * new_employee = employee_form(employee_head);
            add_employee(&employee_head, new_employee);
            printf("\033[1;32m Employee added successfully\033[m\n");
            break;
        }

            // show
        case 12: {
            int id = 0;
            printf("enter id >>> ");
            scanf("%d", &id);
            getchar();
            show_department(department_head, id);
            break;
        }
        case 22: {
            int id = 0;
            printf("enter id >>> ");
            scanf("%d", &id);
            getchar();
            show_role(role_head, id);
            break;
        }
        case 32: {
            int id = 0;
            printf("enter id >>> ");
            scanf("%d", &id);
            getchar();
            show_employee(employee_head, id);
            break;
        }

            // list
        case 13: {
            list_departments(department_head);
            break;
        }
        case 23: {
            list_roles(role_head);
            break;
        }
        case 33: {
            list_employees(employee_head);
            break;
        }

            // update
        case 14: {
            int id = 0;
            printf("enter id >>> ");
            scanf("%d", &id);
            getchar();
            modify_department(&department_head, id);
            break;
        }
        case 24: {
            int id = 0;
            printf("enter id >>> ");
            scanf("%d", &id);
            getchar();
            modify_role(&role_head, id);
            break;
        }
        case 34: {
            int id = 0;
            printf("enter id >>> ");
            scanf("%d", &id);
            getchar();
            modify_employee(&employee_head, id);
            break;
        }

            // delete
        case 15: {
            int id = 0;
            printf("enter id >>> ");
            scanf("%d", &id);
            getchar();
            delete_department(&department_head, id);
            break;
        }
        case 25: {
            int id = 0;
            printf("enter id >>> ");
            scanf("%d", &id);
            getchar();
            delete_role(&role_head, id);
            break;
        }
        case 35: {
            int id = 0;
            printf("enter id >>> ");
            scanf("%d", &id);
            getchar();
            delete_employee(&employee_head, id);
            break;
        }

        case 41:
            listing_per_role(employee_head, role_head);
            break;
        case 42:
            listing_per_department(employee_head, department_head);
            break;
        
        default:
            break;
        }
        save(employee_head, department_head, role_head);

        printf("\nYour choice (-1 Options) >>> ");
        scanf("%d", &choice);
        printf("\n");
        getchar();

    } while(choice != 0);

    printf("\n\033[1;4;43m All modifications are save automatically. Exiting the program \033[m\n\n");

}