#include "init.h"
#include "readfile.h"
// #include "department.h"
// #include "employee.h"

void init(t_employee ** employee_head, t_department ** department_head, t_role ** role_head) {
    int success = 1;
    FileContent * departments_obj = (FileContent*) malloc(sizeof(FileContent));
    FileContent * roles_obj = (FileContent*) malloc(sizeof(FileContent));
    FileContent * employees_obj = (FileContent*) malloc(sizeof(FileContent));
    
    // departments management
    FILE * departments_repos;
    if( ( departments_repos = fopen("./repository/departments.db", "r") ) ) {

        departments_obj = readfile(departments_repos);
        fclose(departments_repos);
        // printf("\033[0;34m [i] departement repository read. \033[m\n");

    } else {
        success = 0;
        printf("\033[0;31m [i] can't open departement repository. \033[m\n");
    }

    if(success) {
        FileContent * current_department = departments_obj;
        while (current_department != NULL) {
            t_department * new_department = (t_department*) malloc(sizeof(t_department));
            new_department->next = NULL;

            current_department->data = strremoveone('\n', current_department->data);
            char * id = strtok(current_department->data, ";");
            char * label = strtok(NULL, ";");

            new_department->id = atoi(id);
            new_department->label = (char*) malloc(sizeof(char));
            new_department->label = label;
            add_department(department_head, new_department);

            current_department = current_department->next;
        }
    }


    // roles management
    FILE * roles_repos;
    if( ( roles_repos = fopen("./repository/roles.db", "r") ) ) {

        roles_obj = readfile(roles_repos);
        fclose(roles_repos);
        // printf("\033[0;34m [i] roles repository read. \033[m\n");

    } else {
        success = 0;
        printf("\033[0;31m [i] can't open roles repository. \033[m\n");
    }

    if(success) {
        FileContent * current_role = roles_obj;
        while (current_role != NULL) {
            t_role * new_role = (t_role*) malloc(sizeof(t_role));
            new_role->next = NULL;

            current_role->data = strremoveone('\n', current_role->data);
            char * id = strtok(current_role->data, ";");
            char * label = strtok(NULL, ";");

            new_role->id = atoi(id);
            new_role->label = (char*) malloc(sizeof(char));
            new_role->label = label;
            add_role(role_head, new_role);
            

            current_role = current_role->next;
        }
    }

    // employees management
    FILE * employees_repos;
    if( ( employees_repos = fopen("./repository/employees.db", "r") ) ) {

        employees_obj = readfile(employees_repos);
        fclose(employees_repos);
        // printf("\033[0;34m [i] employees repository read. \033[m\n");
    } else {
        success = 0;
        printf("\033[0;31m [i] can't open employees repository. \033[m\n");
    }

    if(success) {
        FileContent * current_employee = employees_obj;
        while (current_employee != NULL) {

            t_employee * new_employee = (t_employee*) malloc(sizeof(t_employee));
            new_employee->next = NULL;


            current_employee->data = strremoveone('\n', current_employee->data);
            new_employee->id = atoi(strtok(current_employee->data, ";"));
            new_employee->firstname = strtok(NULL, ";");
            new_employee->lastname = strtok(NULL, ";");
            new_employee->address = strtok(NULL, ";");
            new_employee->phone = strtok(NULL, ";");
            new_employee->email = strtok(NULL, ";");
            new_employee->department_id = atoi(strtok(NULL, ";"));
            new_employee->role_id = atoi(strtok(NULL, ";"));

            add_employee(employee_head, new_employee);

            current_employee = current_employee->next;
        }
    }


    if(success == 0) {
        free(roles_obj);
        free(employees_obj);
        free(departments_obj);

        printf("\033[1;31m [error] errors occur reading repositories files. urgent exit. \033[m\n");
        exit(EXIT_FAILURE);
    }
}

void save(t_employee * employee_head, t_department * department_head, t_role * role_head) {

    FILE * departments_repos = fopen("./repository/departments.db", "w");
    FILE * roles_repos = fopen("./repository/roles.db", "w");
    FILE * employees_repos = fopen("./repository/employees.db", "w");

    // employees
    t_employee *current_employee = employee_head;
    while (current_employee != NULL)
    {
        fprintf(employees_repos, "%d;%s;%s;%s;%s;%s;%d;%d\n", 
            current_employee->id, 
            current_employee->firstname,
            current_employee->lastname,
            current_employee->address,
            current_employee->phone,
            current_employee->email,
            current_employee->department_id,
            current_employee->role_id
        );
        current_employee = current_employee->next;
    }
    // printf("\033[0;34m [i] employees repository saved. \033[m\n");
    

    // roles
    t_role *current_role = role_head;
    while (current_role != NULL)
    {
        fprintf(roles_repos, "%d;%s\n", current_role->id, current_role->label);
        current_role = current_role->next;
    }
    // printf("\033[0;34m [i] roles repository saved. \033[m\n");

    // departments
    t_department *current_department = department_head;
    while (current_department != NULL)
    {
        fprintf(departments_repos, "%d;%s\n", current_department->id, current_department->label);
        current_department = current_department->next;
    }
    // printf("\033[0;34m [i] departements repository saved. \033[m\n");

    fclose(employees_repos);
    fclose(departments_repos);
    fclose(roles_repos);
}

char * strremoveone(char find, char * str) {
    int len = strlen(str);
    char * temp = malloc(len * sizeof(char));
    strcpy(temp, str);
    for(int i=0; i < len; i++) {
        if(temp[i] == find) {
            for(int k=i; k < len; k++) {
                temp[k] = temp[k+1];
            }
            len--;
            break;
        }
    }
    return temp;
}