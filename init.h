#ifndef init_h
#define init_h

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include "employee.h"
    #include "department.h"
    #include "role.h"

    void init(t_employee ** , t_department **, t_role **);
    void save(t_employee * employee_head, t_department * department_head, t_role * role_head);
    char * strremoveone(char find, char * str);
    void main_loop();

#endif // !init_h